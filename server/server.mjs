import express from "express";
// const express = require('express');
const app = express();
const PORT = 3000;
app.use(express.json()); // JSONペイロードの解析のため
app.use(express.urlencoded({ extended: true })); // URLエンコードされたデータの解析のため
// import { openaiAction } from "./controler/openai.js";
import { imitDbAction, todoAllActions, getTasksAll, addTableAction } from "./controler/todo.js";
// import { initDb, todoActionsDb, getTasksDb } from "./model/todo.js";

const allowCrossDomain = (req,res, next) => {
    res.header('Access-Control-Allow-Origin', 'http://localhost:5173')
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
    res.header(
      'Access-Control-Allow-Headers',
      'Content-Type, Authorization, access_token'
    )
    if(res.method === 'OPTIONS') {
      res.send(200)
    }else {
      next()
    }
}
app.use(allowCrossDomain)

app.get('/', (req, res) => {
    res.send('Hello, World!');
});

app.get('/init_todo', imitDbAction);
app.post('/todo_actions', todoAllActions);
app.get('/get_tasks_all', getTasksAll);
// app.post('/openai', openaiAction);
app.get('/add_table', addTableAction);


app.listen(PORT, () => {
    console.log(`Server is running on http://localhost:${PORT}`);
});
