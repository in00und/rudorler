import fetch from "node-fetch";
import { OpenAIAPI  } from "openai";
const API_ENDPOINT = "https://api.openai.com/v1/chat/completions";
const API_KEY = "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";
const openai = new OpenAIAPI({ key: API_KEY })

async function askGPT(promptText) {
  const gptRequestPayload = {
    engine: "text-davinci-002", // エンジンの種類によって変更することができます
    prompt: promptText,
    max_tokens: 100,
  }
  try {
    const gptResponse = await openai.createCompletion(gptRequestPayload);
    return gptResponse.data.choices[0].text.trim();
  } catch (error) {
    console.error(error);
    return "An error occurred.";
  }
}

export async function* openaiAction(req,res) {
  (async () => {
    const answer = await askGPT("What is the capital of France?");
    console.log(answer); // 期待される出力: "Paris"
  })();

  // const response = await fetch(API_ENDPOINT, {
  //   method: "POST",
  //   headers: {
  //       "Content-Type": "application/json",
  //       "Authorization": `Bearer ${API_KEY}`
  //   },
  //   body: JSON.stringify({
  //       "model": "gpt-3.5-turbo",
  //       "messages": [{"role": "user", "content": text}],
  //       temperature: 0.7
  //    })
  //   }
  // )
    // console.log(response)
  return res.json({ "mssage" : "hello world" })
}
