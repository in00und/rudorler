import _sqlite3 from "sqlite3";
const  sqlite3 = _sqlite3.verbose();

// データベースファイル'./task.sqlite'に接続
let db = new sqlite3.Database('./todo.sqlite', sqlite3.OPEN_READWRITE | sqlite3.OPEN_CREATE, (err) => {
    if (err) {
     console.error("pipoikoppk");
      return console.error(err.message);
    }
    console.log('Connected to the SQlite database.');
  });
  
  export const initDb = (req,res) => {
    db.serialize(() => {
      db.run(`CREATE TABLE IF NOT EXISTS todos(id INTEGER PRIMARY KEY, title TEXT, body TEXT, status TEXT,created_at DATETIME DEFAULT CURRENT_TIMESTAMP)`);
      return res.json({ message: "CREATE TABLE todos" });
    });
  }

  export const addTable = (req,res) => {
    db.serialize(() => {
      db.run(`ALTER TABLE todos ADD COLUMN selectModel text`);
      return res.json({ message: "add TABLE todos" });
    });
  }

  export const todoActionsDb = (req,res) => {
     const { actionType, title, body, id } = req.body;
     let msg = "";
    switch (actionType) {
      case "add":    
        db.serialize(() => {
          try {
            db.run(`INSERT INTO todos(title, body, status) VALUES(?,?,?)`, [title,body,"run"]); 
            msg = "success add action";
          } catch (error) {
            msg = "error add todo none";
            console.log('add todo none');
            console.log(error);
          }
        });
        break;
      case "update":
        db.run(`UPDATE todos SET title = ?, body = ? WHERE id = ?`, [title, body, req.body.id], (err) => {
          if (err) {
            return console.error(err.message);
          }
          console.log('Row updated.');
        });
        break;
      case "rerun":
        db.run(`UPDATE todos SET status = ? WHERE id = ?`, ["run", id], (err) => {
          if (err) {
            return console.error(err.message);
          }
          console.log('Row updated.');
        });
        break;
      case "archive":
        db.run(`UPDATE todos SET status = ? WHERE id = ?`, ["archive", id], (err) => {
          if (err) {
            return console.error(err.message);
          }
          console.log('Row updated.');
        });
        break;
      case "delete":
        db.run(`UPDATE todos SET status = ? WHERE id = ?`, ["off", id], (err) => {
          if (err) {
            return console.error(err.message);
          }
          console.log('Row updated.');
        });
        break;
    }
    return res.json({ message: msg });
  }
  
  export const getTasksDb = (req,res) => {
    db.serialize(() => {
      db.all("SELECT * FROM todos", (err, rows) => {
        if (err) {
          console.error(err.message);
          res.status(500).send([]);
        } else {
          const list = rows.filter((item) => (item?.status !== "off"))
          res.send(list ?? []);
        }
      });
    });
  }
  