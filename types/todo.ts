
export interface Todo {
  id: string
  title: string
  body: string
  create_at: string
  status: string
}

export type Todos = Todo[]
