import { createApp } from 'vue'
import './clarity.css'
import './style.css'
import App from './App.vue'
import { createRouter } from "./router";
import { store, key } from "./store";

createApp(App)
.use(createRouter('history'))
.use(store,key)
.mount('#app')
