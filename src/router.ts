import * as VueRouter  from "vue-router";

import Index from "./pages/Index.vue";
import About from "./pages/About.vue";
import Post from "./pages/Post.vue";
import MakeStruct from "./pages/MakeStruct.vue";

const routes = [
    { path: '/', name: "Top", component: Index },
    { path: '/about',name: "about", component: About },
    { path: '/post',name: "post", component: Post },
    { path: '/makeStruct',name: "makeStruct", component: MakeStruct },
  ]

export const createRouter = (type: "memory" | "history") => {
  const history = type === "memory" ? VueRouter.createMemoryHistory() : VueRouter.createWebHistory();
  return VueRouter.createRouter({ history, routes });
};
