
//  ユーザー情報
//  目的達成度｜実行総数｜自己評価の変化における表示情報フロー自覚

export interface User {
  id: string
  name: string
  attainmentRate: string
  totalrun: string
  level: string
}

export interface EnvResource {
  toolResource: string
  humanResource: string
  hrInfo: string
  toolSystemInfo: string
  financialAffairs: string
  timeResource: string
  knowledgeAndSkill: string
}

export interface State {
  user: User
  envResource: EnvResource

  isLoading: boolean
}

export const user = ({
    namespace: true,
    state: () => ({
      user: {
        id: 'userId001',
        attainmentRate: '20',
        totalrun: '120',
        level: '1-10'
      },
      envResource: {
        toolResource: '0.1',
        humanResource: '0.1',
        hrInfo: '0.1',
        toolSystemInfo: '0.1',
        financialAffairs: '0.1',
        timeResource: '0.1',
        knowledgeAndSkill: '0.1'
      },
      isLoading: false
    }),
    mutations: {
      setUser: ((state, user: User ) => {
        state.user = user
      }),
      setEnvResource: ((state, envResource: EnvResource) => {
        state.envResource = envResource
      }),
      setLoading: ((state,  payload: boolean) => {
        state.isLoading = payload
      })
    },
    actions: {
      setUserAction: (({ commit },  user: User) => {
         if(user) {
          commit('setUser',user)
        }
      }),
      setEnvResourceAction: (({ commit }, envResource: EnvResource) => {
        if(envResource) {
          commit('setEnvResource',envResource)
        }
      }),
      // async addTodoAction({ commit, state, dispatch }, user: User) {}
    }
})
