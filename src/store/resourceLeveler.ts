
export interface MakeLeveler {
  id: string
  name: string
  situationTag: string
  value: string
}

export type MakeLevelList = MakeLeveler[]

export interface ResourceLeveler {
  id: string
  title: string
  body: string
  actionType?: string
  makeLevelList: MakeLevelList
  status: string
}

export type ResourceLevelList = ResourceLeveler[]

export interface ResourceInfo {
  level: string
  type: string
}

export interface State {
  resourceLevelList: ResourceLevelList[]
  resourceInfo: ResourceInfo,
  isLoading: boolean
}

export const resourcelevelers = ({
    namespace: true,
    state: () => ({
        resourceLevelList: [],
        resourceInfo: {
          level: '1',
          type: '2'
        },
        isLoading: false
    }),
    mutations: {
      setResourceLeveler: ((state, resourceLeveler: ResourceLeveler[]) => {
        state.resourceLevelList = resourceLeveler
      }),
      addResourceLeveler: ((state, todo: ResourceLeveler) => {
        state.resourceLevelList = [...state.resourceLevelList,todo]
      }),
      setResourceInfo: ((state, resourceInfo: RresourceInfo) => {
        state.resourceInfo = resourceInfo
      }),
      setLoading: ((state, payload: boolean) => {
        state.isLoading = payload
      })
    },
    actions: {
      fetchResourceLeveler: ( async({ commit }) => {
        commit('setLoading',true)
        try {
          const res = await fetch('/resourceleveler.json')
          const resourceLevelList = await  res.json()
          commit('setResourceLeveler',resourceLevelList)
        } catch (error) {
          console.log('Error fetching todos:',error)
        }finally {
          commit('setLoading',false)
        }
      }),
      setResourceInfo: ( async({ commit }, item: RresourceInfo ) => {
        commit('setResourceInfo',item)
      }),
      setaddResourceLevelAction: (({ commit }, item: ResourceLeveler) => {
        if(item) {
          commit('addResourceLeveler',item)
        }
      }),
      // addResourcelevelListAction: ( async ({ commit, state, dispatch }, item: Todo) => {
      //     commit('setLoading',true)
      //     try {
      //       const res = await fetch('http://localhost:3000/todo_actions',{
      //         method: "POST",
      //         headers: {
      //           "Content-Type": "application/json"
      //         },
      //         body: JSON.stringify(item)
      //       })
      //       const todos = await res.json()
      //       commit('setTodos',todos)
      //     } catch (error) {
      //       console.log('Error fetching todos:',error)
      //     }finally {
      //       dispatch('fetchTodos')
      //       commit('setLoading',false)
      //     }
      //   })
    }
})
