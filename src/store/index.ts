import { InjectionKey } from 'vue';
import { createStore, Store, useStore as baseUseStore } from 'vuex';

import { todos, State as TodoState} from "./todo";
import { user, State as UserState} from "./userInfo";
import { resourcelevelers, State as ResourcelevelerState} from "./resourceLeveler";

interface State {
  todos: TodoState
  userInfo: UserState
  resourcelevelers: ResourcelevelerState
}

export const store = createStore({
  modules: {
    todos: todos,
    userInfo: user,
    resourcelevelers: resourcelevelers
  }
})

export const key: InjectionKey<Store<State>> = Symbol('vue-store');

// Custom useStore function
export function useStore() {
  return baseUseStore(key);
}
 