
export interface Todo {
  id: string
  title: string
  body: string
  status: string
  actionType?: string
  connectId?: string
  selectModel?: string
  created_at?: string 
}

export type TodoList = Todo[]

export interface State {
  todoList: Todo[]
  isLoading: boolean
}

export const todos = ({
    namespace: true,
    state: () => ({
        todoList: [],
        isLoading: false
    }),
    mutations: {
      setTodos: ((state, todos: Todo[]) => {
          state.todoList = todos
      }),
      addTodos: ((state, todo: Todo) => {
        state.todoList = [...state.todoList,todo]
      }),
      setLoading: ((state, payload: boolean) => {
          state.isLoading = payload
      })
    },
    actions: {
      fetchTodos: ( async({ commit }) => {
        commit('setLoading',true)
        try {
          const res = await fetch('http://localhost:3000/get_tasks_all')
          const todos = await  res.json()
          commit('setTodos',todos)
        } catch (error) {
          console.log('Error fetching todos:',error)
        }finally {
          commit('setLoading',false)
        }
      }),
      setTodoAction: (({ commit }, item: Todo) => {
        if(item) {
          commit('addTodos',item)
        }
      }),
      addTodoAction: ( async ({ commit, state, dispatch }, item: Todo) => {
        commit('setLoading',true)
        try {
          const res = await fetch('http://localhost:3000/todo_actions',{
            method: "POST",
            headers: {
              "Content-Type": "application/json"
            },
            body: JSON.stringify(item)
          })
          const todos = await res.json()
          commit('setTodos',todos)
        } catch (error) {
          console.log('Error fetching todos:',error)
        }finally {
          dispatch('fetchTodos')
          commit('setLoading',false)
        }
      })
    }
})
